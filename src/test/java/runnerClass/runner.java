package runnerClass;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import com.cucumber.listener.ExtentProperties;
import com.cucumber.listener.Reporter;
import cucumber.api.junit.Cucumber;
import cucumber.api.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/resources/FeatureFiles/features.feature",monochrome = true,
plugin = {"pretty","json:target/cucumber.json","html:target/cucumber","com.cucumber.listener.ExtentCucumberFormatter:"}, glue="stepDefinition", dryRun = false, tags = {"@Assignment"})
public class runner {

 @BeforeClass
 public static void setup() {
	 Date now = new Date();
	 SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy_HH-mm-ss");
	 String time = dateFormat.format(now);
	 String path = "results/"+time;
	 File dir = new File(path);
	 if(!dir.exists())
	 {
		 try {
			 dir.mkdir(); 
		 }catch(Exception e) {
			 System.out.println(e.getMessage());
		 }
		System.setProperty("executionFolderPath", path);
		System.setProperty("logFilePath", path+"/logs");
	 }
	 ExtentProperties extentProperties = ExtentProperties.INSTANCE;
	 extentProperties.setReportPath(System.getProperty("executionFolderPath")+"/"+"CucumberReport"+".html");
	 
 }
	
 @AfterClass
 public static void writeExtentReport() {
	 Reporter.loadXMLConfig(new File("extent-config.xml"));
 }
	
}
