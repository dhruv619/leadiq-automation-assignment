package supportLibraries;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import junit.framework.Assert;

public class KeywordLibrary {
	
	final static Logger logger = Logger.getLogger("KeywordLibrary");
	public WebDriver driver;
	public String parentWindow;
	Properties prop;
	FileInputStream fileInput; 

	public void loadPropertiesFile()
	{
		try {
			fileInput = new FileInputStream("./GlobalSettings.properties");
			prop = new Properties();
			prop.load(fileInput);
			logger.info("GlobalSeetings property file got loaded successfully");
		} catch (Exception e) {
			logger.error("Failed to load GlobalSeetings property file : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	public void setup()
	{
		loadPropertiesFile();
		setUpDriverExePath();
		initialize_Log4j_File();
		try {
			driver = new ChromeDriver();
			logger.info("Chrome Browser got opened successfully");
		}catch(Exception e) {
			logger.info("Chrome Browser didn't get opened because :"+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void initialize_Log4j_File()
	{
		PropertyConfigurator.configure("log4j.properties");
	}
	
	public void setUpDriverExePath()
	{
		System.setProperty("webdriver.chrome.driver", prop.getProperty("chromeDriverPath"));
		logger.info("Chrome Driver Path has been setup successfully");
	}
	
	public void open_URL(String Website)
	{
		try {
			driver.get(Website);
			logger.info(Website+" URL got opened successfully");
		}catch(Exception e) {
			logger.error("Unable to open URL : "+Website+" because of :"+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public WebElement findElement(String by, String locator)
	{
		WebElement webElement = null;
		try {
			if(by.equalsIgnoreCase("id"))
			{
				webElement = driver.findElement(By.id(locator));
			}
			else if(by.equalsIgnoreCase("name"))
			{
				webElement = driver.findElement(By.name(locator));
			}
			else if(by.equalsIgnoreCase("xpath"))
			{
				webElement = driver.findElement(By.xpath(locator));
			}
			else if(by.equalsIgnoreCase("linktext"))
			{
				webElement = driver.findElement(By.linkText(locator));
			}
			else if(by.equalsIgnoreCase("partiallinktext"))
			{
				webElement = driver.findElement(By.partialLinkText(locator));
			}
			else
			{
				logger.error("Invalid value for 'BY'");
			}
		}catch(Exception e){
			logger.error("Failed while finding the element : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
		return webElement;
	}
	
	public void clickElement(String by, String locator)
	{
		try {
			findElement(by,locator).click();
			logger.info("Clicked on "+locator+" using "+by);
		}catch(Exception e) {
			logger.error("Unable to click "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void waitUntilElementClickable(By by, long timeOutInSeconds)
	{
		new WebDriverWait(driver,timeOutInSeconds).until(ExpectedConditions.elementToBeClickable(by));
	}
	
	public void waitUntilElementIsClickable(String by, String locator, long timeout)
	{
		try {
		
			if(by.equalsIgnoreCase("id"))
			{
				waitUntilElementClickable(By.id(locator),timeout);
			}
			else if(by.equalsIgnoreCase("name"))
			{
				waitUntilElementClickable(By.name(locator),timeout);
			}
			else if(by.equalsIgnoreCase("xpath"))
			{
				waitUntilElementClickable(By.xpath(locator),timeout);
			}
			else if(by.equalsIgnoreCase("linktext"))
			{
				waitUntilElementClickable(By.linkText(locator),timeout);
			}
			else if(by.equalsIgnoreCase("partiallinktext"))
			{
				waitUntilElementClickable(By.partialLinkText(locator),timeout);
			}
			else
			{
				logger.error("Invalid value for 'BY'");
			}
		}catch(Exception e){
			logger.error("Failed while waiting for element to be clickable : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void waitUntilElementVisible(By by, long timeOutInSeconds)
	{
		new WebDriverWait(driver,timeOutInSeconds).until(ExpectedConditions.presenceOfElementLocated((by)));
	}
	
	public void waitUntilElementIsVisible(String by, String locator, long timeout)
	{
		try {
		
			if(by.equalsIgnoreCase("id"))
			{
				waitUntilElementVisible(By.id(locator),timeout);
			}
			else if(by.equalsIgnoreCase("name"))
			{
				waitUntilElementVisible(By.name(locator),timeout);
			}
			else if(by.equalsIgnoreCase("xpath"))
			{
				waitUntilElementVisible(By.xpath(locator),timeout);
			}
			else if(by.equalsIgnoreCase("linktext"))
			{
				waitUntilElementVisible(By.linkText(locator),timeout);
			}
			else if(by.equalsIgnoreCase("partiallinktext"))
			{
				waitUntilElementVisible(By.partialLinkText(locator),timeout);
			}
			else
			{
				logger.error("Invalid value for 'BY'");
			}
		}catch(Exception e){
			logger.error("Failed while wiating for element to be visible : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void addExtension()
	{
		try {
			setUpDriverExePath();
			logger.info("Chrome Driver Path has been setup successfully");
			ChromeOptions options = new ChromeOptions();
			options.addExtensions(new File(prop.getProperty("crxFilePath")));
			logger.info("LeadIQ .crx file extension has been added successfully");
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			driver = new ChromeDriver(capabilities);
			logger.info("Chrome browser has been opened successfully");
		}catch(Exception e) {
			logger.error("Failed to add LeadIQ extension to chrome : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void clickSikuli(String path) throws FindFailed
	{
		try {
			Screen screen = new Screen();
			Pattern extension = new Pattern(path);
			screen.click(extension);
			logger.info("Clicked on button through sikuli successfully");
		}catch(Exception e) {
			logger.error("Failed to click on button through sikuli : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void maximizeWindow()
	{
		try {
			driver.manage().window().maximize();
			logger.info("window got maximized successfully");
		}catch(Exception e)
		{
			logger.error("window maximization was unsuccessfull");
			Assert.assertEquals(true, false);
		}
	}
	
	public void switch_Window()
	{
		try {
			Set<String> handles =  driver.getWindowHandles();
			System.out.println(handles);
			for(String windowHandle  : handles)
		       {
		       if(!windowHandle.equals(parentWindow))
		          {
		    	   driver.switchTo().window(windowHandle);
		          }
		       }
			logger.info("Switched window successfully");
		}catch(Exception e) {
			logger.error("Failed to switch window : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
	
	public void storeParentWindow()
	{
		parentWindow = driver.getWindowHandle();
		System.out.println("Parent Window :"+parentWindow);
	}
	
	public void exit()
	{
		try {
			driver.close();
			logger.info("driver got closed successfully");
		}catch(Exception e)
		{
			logger.error("driver close unsuccessfull : "+e.getMessage());
			Assert.assertEquals(true, false);
		}
	}
}
