package supportLibraries;

public class FunctionLibrary {
	
	protected KeywordLibrary kwdlib = new KeywordLibrary();
	
	public void inputText(String by, String locator, String text)
	{
		try {
			kwdlib.findElement(by,locator).click();
			kwdlib.findElement(by,locator).clear();
			kwdlib.findElement(by,locator).sendKeys(text);
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}


}
