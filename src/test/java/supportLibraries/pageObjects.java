package supportLibraries;

public class pageObjects {
	
	//Signup Page Elements
	public static String txb_First_Name = ".//*[@placeholder = 'First Name']";
	public static String txb_Last_Name = ".//*[@placeholder = 'Last Name']";
	public static String txb_Email = ".//*[@placeholder = 'Email']";
	public static String txb_Company_Name = ".//*[@placeholder = 'Company Name']";
	public static String txb_Password = ".//*[@placeholder = 'Password']";
	public static String txb_Cnf_Password = ".//*[@placeholder = 'Confirm Password']";
	public static String btn_Submit = ".//div[@class = 'container flex-center']";
	
	//chrome Webstore Page
	public static String btn_addToChrome = ".//div[@class = 'h-e-f-Ra-c e-f-oh-Md-zb-k']//div[@class = 'g-c-R  webstore-test-button-label']";

	//chrome-extension downloader page
	public static String txb_EnterExtensionURL = ".//*[@name = 'extension']";
	public static String btn_downloadExtension = ".//*[text() = 'Download extension']";
	
	//LeadIQ SignIn Page
	public static String txb_LeadIQ_EmailID = ".//input[@type = 'email']";
	public static String txb_LeadIQ_Password = ".//input[@type = 'password']";
	public static String btn_LeadIQ_SignIn = ".//div[@class = 'el-form-item__content']/div[1]/div[1]";
}
