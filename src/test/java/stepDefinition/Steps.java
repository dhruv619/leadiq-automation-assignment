package stepDefinition;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import com.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import supportLibraries.FunctionLibrary;
import supportLibraries.pageObjects;

public class Steps extends FunctionLibrary{
	Properties prop;
	FileInputStream fileInput; 
	
	public Steps()
	{
		try {
			fileInput = new FileInputStream("./GlobalSettings.properties");
			prop = new Properties();
			prop.load(fileInput);
		} catch (Exception e) {
			Assert.assertEquals(true, false);
		}
	}
	
	@Given("^Open chrome browser and go to \"(.*?)\"$")
	public void open_chrome_browser_and_go_to(String site) throws Throwable {
		kwdlib.setup();
		Reporter.addStepLog("Initial Setup done Successfully");
		kwdlib.open_URL(site);
		Reporter.addStepLog("URL got openend Successfully");
		kwdlib.waitUntilElementIsClickable("xpath",pageObjects.btn_Submit,20);
		kwdlib.clickElement("xpath", pageObjects.txb_First_Name);
		inputText("xpath",pageObjects.txb_First_Name,prop.getProperty("firstName"));
		Reporter.addStepLog("Entered First Name in Textbox successfully");
		kwdlib.clickElement("xpath", pageObjects.txb_Last_Name);
		inputText("xpath",pageObjects.txb_Last_Name,prop.getProperty("lastName"));
		Reporter.addStepLog("Entered Last Name in Textbox successfully");
		kwdlib.clickElement("xpath", pageObjects.txb_Email);
		inputText("xpath",pageObjects.txb_Email,prop.getProperty("email"));
		Reporter.addStepLog("Entered Email in Textbox successfully");
		kwdlib.clickElement("xpath", pageObjects.txb_Company_Name);
		inputText("xpath",pageObjects.txb_Company_Name,prop.getProperty("companyName"));
		Reporter.addStepLog("Entered Company Name in Textbox successfully");
		kwdlib.clickElement("xpath", pageObjects.txb_Password);
		inputText("xpath",pageObjects.txb_Password,prop.getProperty("password"));
		Reporter.addStepLog("Entered Password in Textbox successfully");
		kwdlib.clickElement("xpath", pageObjects.txb_Cnf_Password);
		inputText("xpath",pageObjects.txb_Cnf_Password,prop.getProperty("password"));
		Reporter.addStepLog("Entered confirm Password in Textbox successfully");
		kwdlib.clickElement("xpath", pageObjects.btn_Submit);
		kwdlib.exit();
	}
	
	@When("^LeadIQ Extension is Installed$")
	public void leadiq_Extension_is_Installed() throws Throwable {
		kwdlib.setup();
		Reporter.addStepLog("Initial Setup done Successfully");
		kwdlib.open_URL("https://chrome-extension-downloader.com/");
		Reporter.addStepLog("URL https://chrome-extension-downloader.com/ got opened Successfully");
		kwdlib.waitUntilElementIsClickable("xpath",pageObjects.txb_EnterExtensionURL,20);
		kwdlib.clickElement("xpath", pageObjects.txb_EnterExtensionURL);
		Reporter.addStepLog("Clicked on element with xpath : .//*[@name = 'extension'] successfully");
		inputText("xpath",pageObjects.txb_EnterExtensionURL,"https://chrome.google.com/webstore/detail/leadiq-lead-capture/befngoippmpmobkkpkdoblkmofpjihnk");
		Reporter.addStepLog("Text entered successfully");
		kwdlib.clickElement("xpath", pageObjects.btn_downloadExtension);
		Reporter.addStepLog("Clicked on element with xpath .//*[text() = 'Download extension'] successfully");
		kwdlib.clickSikuli(".\\src\\test\\resources\\btn_keep.png");
		kwdlib.exit();
	}

	@When("^Open LeadIQ from its Extension$")
	public void open_LeadIQ_from_its_Extension() throws Throwable {
		kwdlib.addExtension();
		Reporter.addStepLog("LeadIQ extension added successfully");
		kwdlib.storeParentWindow();
		Reporter.addStepLog("Parent window stored successfully");
		kwdlib.clickSikuli(".\\src\\test\\resources\\extension.png");
		kwdlib.switch_Window();
		Reporter.addStepLog("Switched to new window successfully");
		kwdlib.maximizeWindow();
	}

}
