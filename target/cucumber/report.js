$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("src/test/resources/FeatureFiles/features.feature");
formatter.feature({
  "line": 1,
  "name": "Automation",
  "description": "",
  "id": "automation",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 3,
  "name": "SignUp on LeadIQ",
  "description": "",
  "id": "automation;signup-on-leadiq",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@Assignment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Open chrome browser and go to \"\u003cWebsite\u003e\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "LeadIQ Extension is Installed",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Open LeadIQ from its Extension",
  "keyword": "And "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "automation;signup-on-leadiq;",
  "rows": [
    {
      "cells": [
        "Website"
      ],
      "line": 10,
      "id": "automation;signup-on-leadiq;;1"
    },
    {
      "cells": [
        "https://account.leadiq.com/app/signup/?referralCode\u003dgoneprospectin"
      ],
      "line": 11,
      "id": "automation;signup-on-leadiq;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "SignUp on LeadIQ",
  "description": "",
  "id": "automation;signup-on-leadiq;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 2,
      "name": "@Assignment"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "Open chrome browser and go to \"https://account.leadiq.com/app/signup/?referralCode\u003dgoneprospectin\"",
  "matchedColumns": [
    0
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "LeadIQ Extension is Installed",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Open LeadIQ from its Extension",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "https://account.leadiq.com/app/signup/?referralCode\u003dgoneprospectin",
      "offset": 31
    }
  ],
  "location": "Steps.open_chrome_browser_and_go_to(String)"
});
formatter.result({
  "duration": 105349332662,
  "status": "passed"
});
formatter.match({
  "location": "Steps.leadiq_Extension_is_Installed()"
});
formatter.result({
  "duration": 47661720869,
  "status": "passed"
});
formatter.match({
  "location": "Steps.open_LeadIQ_from_its_Extension()"
});
formatter.result({
  "duration": 94443022687,
  "status": "passed"
});
});